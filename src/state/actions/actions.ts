import { TagItem } from "../model/TagItem";
import { ActionTypes, ActionCreators } from "./actionTypes";

import { tagRequestManager } from "../api/tagRequestManager";
import { seriesRequestManager, ISeriesData } from "../api/seriesRequestManager";

export const getLoadInitAction = (): ILoadInitAction => {
    return {type: ActionTypes.LOAD_INIT};
};

export const getSetTagAction = (tagId: string): ISetTagAction => {
    return {type: ActionTypes.SET_TAG, tagId};
};

export const setTagAction = (tagId: string) => {
    return (dispatch: Dispatch<Action>, getState) => {
        const state = getState();

        // initiate resource request
        const { activeTag, fromDate, toDate } = state.controls;
        seriesRequestManager.fetchSeries(tagId, fromDate, toDate);

        // dispatch LOAD_INIT action currently no-op
        return dispatch(getSetTagAction(tagId));
    };
};

export const clearTagAction = (): IClearTagAction => {
    return {type: ActionTypes.CLEAR_TAG};
};

export const loadInitAction = () => {
    return (dispatch: Dispatch<Action>, getState) => {
        const state = getState();

        // initiate resource request
        tagRequestManager.fetchTags().then(() => {
            if ( state.controls.activeTag ) {
                const { activeTag, fromDate, toDate } = state.controls;
                return seriesRequestManager.fetchSeries(activeTag, fromDate, toDate);
            }
        });

        // dispatch LOAD_INIT action currently no-op
        return dispatch(getLoadInitAction());
    };
};

export const recieveInitAction = (tags: TagItem[]): IRecieveInitAction => {
    return {type: ActionTypes.RECIEVE_INIT, tags};
};

export const getUpdateRangeAction = (fromDate: Date, toDate: Date): IUpdateRangeAction => {
    return {type: ActionTypes.UPDATE_RANGE, fromDate, toDate};
};

export const updateRangeAction = (tagId: string, fromDate: Date, toDate: Date) => {
    return (dispatch: Dispatch<Action>) => {
        // initiate resource request
        seriesRequestManager.fetchSeries(tagId, fromDate, toDate);

        return dispatch(getUpdateRangeAction(fromDate, toDate));
    };
};

export const recieveDataAction = (data: ISeriesData): IRecieveDataAction => {
    return {type: ActionTypes.RECIEVE_DATA, data};
};

export const toggleFeatureFilterAction = (feature: string): ISetFeatureFilterAction => {
    return {type: ActionTypes.SET_FEATURE_FILTER, feature};
};

export const actionCreators = {
    setTagAction,
    clearTagAction,
    loadInitAction,
    recieveInitAction,
    updateRangeAction,
    recieveDataAction,
    toggleFeatureFilterAction,
};
