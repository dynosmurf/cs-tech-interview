import { Action } from "redux";
import { TagItem } from "../model/TagItem";

export const ActionTypes = {
    LOAD_INIT: "LOAD_INIT",
    RECIEVE_INIT: "RECIEVE_INIT",
    UPDATE_RANGE: "UPDATE_RANGE",
    RECIEVE_DATA: "RECIEVE_DATA",
    SET_FEATURE_FILTER: "SET_FEATURE_FILTER",
    SET_TAG: "SET_TAG",
    CLEAR_TAG: "CLEAR_TAG",
};

// TODO:
// https://github.com/palantir/tslint/issues/2740
// Not sure about this
export type ILoadInitAction = Action;
export type IClearTagAction = Action;

export interface ISetTagAction extends Action {
    tagId: string;
}

export interface IRecieveInitAction extends Action {
    tags: TagItem[];
}

export interface IUpdateRangeAction extends Action {
    fromDate: Date;
    toDate: Date;
}

export interface IRecieveDataAction extends Action {
    data: {
        fromDate: Date;
        toDate: Date;
        points: any[];  // TODO: more specific
    };
}

export interface ISetFeatureFilterAction extends Action {
    activeFeatures: string[];
}
