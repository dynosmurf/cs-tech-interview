import { subDays } from "date-fns";
import { TagItem } from "../models/TagItem";
import { DataPoint } from "../models/DataSeries";

export interface IState {
    tags: {
        [tagId: string]: TagItem;
    };
    series: {
        [tagId: string]: {
            [ts: string]: DataPoint;
        },
    };
    activeFeatures: string[];
    controls: {
        fromDate: Date;
        toDate: Date;
        activeTag: string;
        loading: boolean;
    };
}

const initialTagFromUrl = new URL(window.location.href).searchParams.get("tag");

export const initState = {
    tags: {},
    series: {},
    features: [],
    activeFeatures: [],
    controls: {
        fromDate: subDays(new Date("2018-12-30T23:57:01Z"), 2),
        toDate: new Date("2018-12-30T23:57:01Z"),
        activeTag: initialTagFromUrl || null,
        loading: true,
    },
};
