import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension/developmentOnly";
import thunk from "redux-thunk";
import { rootReducer } from "../reducers/rootReducer";
import * as aTypes from "../actions/actionTypes";

const urlModifier = (s: any) => (next: any) => (action: any ) => {
    const result = next(action);
    if (aTypes.ActionTypes.CLEAR_TAG === action.type) {
        history.pushState({}, "Tags", "/");
    }
    if (aTypes.ActionTypes.SET_TAG === action.type) {
        history.pushState({}, "Details", `/?tag=${action.tagId}`);
    }
    return result;
};

export const configureStore = () => {
    if (process.env.NODE_ENV === "production") {
        return createStore(
            rootReducer,
            applyMiddleware(thunk),
        );
    } else {
        return createStore(
            rootReducer,
            composeWithDevTools(
                applyMiddleware(thunk, urlModifier),
            ),
        );
    }
};

export const store = configureStore();
