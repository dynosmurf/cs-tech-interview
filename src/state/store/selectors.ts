import * as dfns from "date-fns";

import { DataPoint } from "../models/DataSeries";
import { TagItem } from "../models/TagItem";

export const selectTags = (state: any ) => {
    const o = [];
    for ( const tagId of Object.keys(state.tags) ) {
        o.push(state.tags[tagId]);
    }
    return o;
};

export const selectCurrentTag = (state: any ) => {
    return state.controls.activeTag;
};

export const selectActiveTags = (state: any ) => {
    const activeFeatures = selectActiveFeatures(state);
    const tags = selectTags(state);
    const o: TagItem[] = [];
    for ( const tag of tags ) {
        for ( const feature of activeFeatures) {
            if (!o.includes(tag) ) {
                if (tag.features.includes(feature)) {
                    o.push(tag);
                }
            }
        }
    }
    return o;
};

export const selectFeatures = (state: any ) => {
    const o = [];
    for ( const tagId of Object.keys(state.tags) ) {
        const tag = state.tags[tagId];
        for ( const feature of tag.features ) {
            if (o.indexOf(feature) < 0) {
                o.push(feature);
            }
        }
    }
    return o.sort();
};

export const selectActiveFeatures = (state: any ) => {
    // dont' want to modify this by mistake.
    return [...state.activeFeatures];
};

export const selectXMin = (state: any ) => {
    return state.controls.fromDate;
};

export const selectXMax = (state: any ) => {
    return state.controls.toDate;
};

export const selectDataPointsInActiveRange = (state: any ): DataPoint[] => {
    const currentTag = selectCurrentTag(state);
    const currentSeries = state.series[currentTag] || {};

    const keys = Object.keys(currentSeries);
    const fromDate = state.controls.fromDate;
    const toDate = state.controls.toDate;

    keys.sort();
    const o = [];
    for (const date of keys) {
        const dateComp = new Date(date);
        if (dfns.isAfter(dateComp, fromDate) && dfns.isBefore( dateComp, toDate)) {
            o.push(currentSeries[date]);
        }
    }

    return o;
};

export const selectCurrentChartType = (state: any ): string => {
    const currentTag = selectCurrentTag(state);
    const tag = state.tags[currentTag];
    if (!tag) {
        return "";
    }

    return tag.unit === "Status" ? "status" : "line";
};
