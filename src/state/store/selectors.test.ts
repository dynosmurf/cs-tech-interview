import * as s from "./selectors";

const mockState = {
    tags: {
        Tag1: {
            tagId: 'Tag1',
            label: 'Power at Meter 1',
            dataType: 'double',
            unit: 'kW',
            isTransient: false,
            features: [
                'power',
                'meter',
                'load',
                'consumption'
            ]
        },
        Tag2: {
            tagId: 'Tag2',
            label: 'Unit 1 Online Status',
            dataType: 'boolean',
            unit: 'Status',
            isTransient: true,
            features: [
                'status',
                'unit'
            ]
        },
    },
    series: {
        Tag4: {
            '2018-12-29T23:59:01Z': {
                observationTS: '2018-12-29T23:59:01Z',
                tagId: 'Tag4',
                value: 728629,
                quality: null,
                ts: '2018-12-29T23:59:01Z'
            },
        }
    },
    activeFeatures: [
        'unit'
    ],
    controls: {
        fromDate: '2018-12-28T23:57:01.000Z',
        toDate: '2018-12-30T23:57:01.000Z',
        activeTag: 'Tag1',
        loading: false
    },
}

describe("api:RequestManager", () => {

    describe("selectTags", () => {
        it("should correctly retrieve tags from state", () => {
            const r = s.selectTags(mockState)
            expect(r[0].tagId).toEqual("Tag1");
        });
    });

    describe("selectActiveTags", () => {
        it("should correctly return active tags from state", () => {
            const r = s.selectActiveTags(mockState)
            expect(r[0].tagId).toEqual("Tag2");
        });
    });

    describe("selectFeatures", () => {
        it("should correctly return features state", () => {
            const r = s.selectFeatures(mockState)
            expect(r).toEqual(["consumption", "load", "meter", "power", "status", "unit"]);
        });
    });

});
