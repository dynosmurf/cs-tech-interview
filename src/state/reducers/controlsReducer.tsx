import { Action } from "redux";
import { initState } from "../store/state";
import * as controlActions from "../actions/actionTypes";

const controlsReducer = (
    state = initState.controls,
    action: Action,
) => {
    switch (action.type) {
        case controlActions.ActionTypes.RECIEVE_INIT:
            return {
                ...state,
                loading: false,
            };
        case controlActions.ActionTypes.CLEAR_TAG:
            return {
                ...state,
                activeTag : null,
            };
        case controlActions.ActionTypes.SET_TAG:
            return {
                ...state,
                activeTag : action.tagId,
            };
        case controlActions.ActionTypes.UPDATE_RANGE:
            const { fromDate, toDate } = action;
            const newState = {
                ...state,
                fromDate,
                toDate,
            };
            return newState;
        default:
            return state;
    }
};

export default controlsReducer;
