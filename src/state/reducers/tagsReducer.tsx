import { Action } from "redux";
import { initState } from "../store/state";
import * as tagActions from "../actions/actionTypes";
import { TagItem } from "../models/TagItem";

const tagsReducer = (
    state = initState.tags,
    action: Action,
) => {
    switch (action.type) {
        case tagActions.ActionTypes.RECIEVE_INIT:
            const newState = {};
            for ( const tag of action.tags ) {
                newState[tag.tagId] = tag;
            }
            return newState;
        default:
            return state;
    }
};

export default tagsReducer;
