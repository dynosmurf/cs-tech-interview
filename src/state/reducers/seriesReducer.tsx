import { Action } from "redux";
import { initState } from "../store/state";
import * as seriesActions from "../actions/actionTypes";

const seriesReducer = (
    state = initState.series,
    action: Action,
) => {
    switch (action.type) {
        case seriesActions.ActionTypes.RECIEVE_DATA:
            const { tagId, series } = action.data;
            const newState = {...state};
            const tagsInState = Object.keys(newState);

            if (!tagsInState.includes(tagId)) {
                newState[tagId] = {};
            } else {
                newState[tagId] = {...newState[tagId]};
            }

            for (const p of series) {
                p.ts = p.observationTS;
                if (p.value === "On") {
                    p.value = 1;
                }
                if (p.value === "Off" ) {
                    p.value = 0;
                }
                newState[tagId][p.ts] = p;
            }
            return newState;

        default:
            return state;
    }
};

export default seriesReducer;
