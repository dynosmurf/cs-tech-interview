import { Action } from "redux";
import { initState } from "../store/state";
import * as featuresActions from "../actions/actionTypes";

const featuresReducer = (
    state = initState.features,
    action: Action,
) => {
    switch (action.type) {
        case featuresActions.ActionTypes.RECIEVE_INIT:
            const newState = [];
            for (const tag of action.tags) {
                for (const f of tag.features ) {
                    if (! newState.includes(f)) {
                        newState.push(f);
                    }
                }
            }
            newState.sort();
            return newState;

        case featuresActions.ActionTypes.SET_FEATURE_FILTER:
            const features = action.feature;
            return [...features];

        default:
            return state;
    }
};

export default featuresReducer;
