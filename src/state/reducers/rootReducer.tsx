import { combineReducers } from "redux";
import { IState } from "../store/configStore";

import tags from "./tagsReducer";
import activeFeatures from "./featuresReducer";
import series from "./seriesReducer";
import controls from "./controlsReducer";

export const rootReducer = combineReducers({
    tags,
    series,
    activeFeatures,
    controls,
});
