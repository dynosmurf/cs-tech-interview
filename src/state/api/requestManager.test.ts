import "enzyme";
import { RequestManager, IResponse, IError } from "./requestManager";

describe("api:RequestManager", () => {

    describe("fetch", () => {
        beforeEach(() => {

            (global as any).fetch = jest.fn().mockImplementation(() => {
                const p = new Promise((resolve, reject) => {
                    resolve({
                        ok: true,
                        Id: "123",
                        json: () => (new Promise((resolve) => {
                            resolve({Id: "123"});
                        })),
                    });
                });

                return p;
            });
        });

        it("should initiate a request to the correct enpoint", (done) => {
            const endpoint = () => "http://www.google.com/api/NotReal";
            const options = {
                timeout: 5000,
                onRecieve: (response: IResponse ) => {
                    console.log(response);
                    expect(response).toEqual({Id: "123"});
                    done();
                },
                onError: (error: IError) => {
                    console.log(error);
                    throw Error(error.message);
                },
            };

            const testRequestManager = new RequestManager(
                endpoint, options,
            );

            testRequestManager.fetch([], {param1 : "test", param2 : "test2"});
        });
    });
});
