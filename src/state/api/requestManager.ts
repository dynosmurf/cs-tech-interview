export interface IResponse {
    data: any;
}

export interface IError {
    message: string;
}

interface IRequestParams {
    [paramName: string]: string;
}

type endpointAccessor = (params: string[]) => string;
type onRecieveCallback = (response: IResponse) => void;
type onErrorCallback = (error: IError) => void;

export interface IRequestManagerOptions {
    timeout: number;
    onRecieve: onRecieveCallback;
    onError: onErrorCallback;
}

export class RequestManager {

    public onRecieve: onRecieveCallback;
    public onError: onErrorCallback;
    private endpoint: endpointAccessor;
    private timeout: number;

    constructor(endpoint: endpointAccessor, options: IRequestManagerOptions) {
        this.endpoint = endpoint;
        this.timeout = options.timeout;
        this.onRecieve = options.onRecieve;
        this.onError = options.onError;
    }

    public fetch(id: string[], params: IRequestParams): Promise<IResponse> {
        const queryString = `?${this.paramsToQueryString(params)}`;
        const dataRequestPromise = fetch(this.endpoint(id) + queryString);
        const timeoutPromise = this.timeoutAfter(this.timeout);

        const resultPromise =  Promise.race([dataRequestPromise, timeoutPromise]).then(
            (response) => {
                return response.json().then((data: any) => {
                    this.handleResponse(data);
                });
            }).catch(this.onError.bind(this));

        return resultPromise;
    }

    private handleResponse(data: IResponse ) {
        this.onRecieve.call(this, data);
        return data;
    }

    private paramsToQueryString(params: IRequestParams): string {
        const qs = new URLSearchParams();
        for (const paramName of Object.keys(params)) {
            qs.append(paramName, params[paramName]);
        }
        return qs.toString();

    }

    private timeoutAfter(milleseconds: number): Promise<any> {
        return new Promise((resolve, reject) => {
            return setTimeout(() => reject(new Error(`Request timed out.`) ), milleseconds);
        });
    }
}
