import * as dfns from "date-fns";
import { appConfig } from "../../appConfig";
import accessFixtureData from "../../fixtures/fixtures";

import { RequestManager } from "./requestManager";
import { store } from "../store/configStore";

import { recieveDataAction } from "../actions/actions";

const debounce = (func, wait, immediate) => {
    let timeout;

    return function executedFunction() {
        const context = this;
        const args = arguments;

        const later = () => {
            timeout = null;
            if (!immediate) {
                func.apply(context, args);
            }
        };

        const callNow = immediate && !timeout;

        clearTimeout(timeout);

        timeout = setTimeout(later, wait);

        if (callNow) {
            func.apply(context, args);
        }
    };
};

export class SeriesRequestManager extends RequestManager {
    constructor(...args) {
        super(...args);

        this.fetchSeries = debounce(this.fetchSeries.bind(this), 500);
    }

    public fetchSeries(tagId: string, fromDate: Date, toDate: Date): Promise<any> {

        if (appConfig.devmode) {
            return new Promise((resolve) => resolve(accessFixtureData("series", tagId)) )
                .then( (data) => {
                    this.onRecieve({
                        tagId,
                        series: data,
                    });
                });
        } else {
            const params = {
                startTS: dfns.format(fromDate, "YYYY-MM-DD[T]HH:mm:ss"),
                endTS: dfns.format(toDate, "YYYY-MM-DD[T]HH:mm:ss"),
            };
            const queryString = `?${this.paramsToQueryString(params)}`;
            const dataRequestPromise = fetch(this.endpoint(tagId) + queryString);
            const timeoutPromise = this.timeoutAfter(this.timeout);

            const resultPromise =  Promise.race([dataRequestPromise, timeoutPromise]).then(
                (response) => {
                    const data = response.json();
                    data.then((d) => {
                        this.onRecieve.call(this, {
                            tagId,
                            series: d,
                        });
                        return d;
                    });
                }).catch(this.onError.bind(this));

            return resultPromise;
        }
    }
}

const endpointAccessor = (tagId) => {
    return `${appConfig.api}${appConfig.endpoints.series(tagId)}`;
};

export const seriesRequestManager = new SeriesRequestManager(endpointAccessor, {
    timeout: appConfig.defaultTimeout,
    onRecieve : (data) => {
        store.dispatch(recieveDataAction(data));
    },
    onError : (error) => {
        // TODO : create action for error handeling, log to console for now
        // console.warn("Error requesting series data.");
    },
});
