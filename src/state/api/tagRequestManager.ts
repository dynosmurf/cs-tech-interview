import { appConfig } from "../../appConfig";

import accessFixtureData from "../../fixtures/fixtures";

import { RequestManager } from "./requestManager";
import { store } from "../store/configStore";

import { recieveInitAction } from "../actions/actions";

export class TagRequestManager extends RequestManager {
    public fetchTags(): Promise<any> {
        if (appConfig.devmode) {
            return new Promise( (resolve) => resolve(accessFixtureData("tags")) ).then( (data) => {
                this.onRecieve(data);
            });
        } else {
            return this.fetch([], {});
        }
    }
}

const endpointAccessor = (tagId) => {
    return `${appConfig.api}${appConfig.endpoints.tags(tagId)}`;
};

export const tagRequestManager = new TagRequestManager(endpointAccessor, {
    timeout: appConfig.defaultTimeout,
    onRecieve : (tags) => {
        store.dispatch(recieveInitAction(tags));
    },
    onError : (error) => {
        // TODO : create action for error handeling, log to console for now
        // console.warn("Error requesting tag data.");
    },
});
