export interface DataPoint {
    ts: "string";
    value: number | string | boolean;
    quality: number | string | boolean | void;
}

export interface DataRange {
    fromDate: Date;
    toDate: Date;
    status: string;
}

export interface DataSeries {
    tagId: string;
    points: {
        [tagId: string]: DataPoint[];
    };
    loadedRanges: DataRange[];
}
