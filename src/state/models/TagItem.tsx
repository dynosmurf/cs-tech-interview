export interface TagItem {
    tagId: string;
    label: string;
    dataType: string;
    unit: string;
    isTransient: boolean;
    features: string[];
}
