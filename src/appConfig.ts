export const appConfig = {
    devmode: false,
    api : "https://cs-mock-timeseries-api.azurewebsites.net",
    endpoints : {
        tags : () => `/api/Tag`,
        series : (tagId: string) => `/api/DataPoint/${tagId}`,
    },
    defaultTimeout: 15000,
};
