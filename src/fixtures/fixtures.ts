const accessFixtureData = (fixtureType, id) => {
    if (fixtureType === "tags") {
        const tags = require("./tags.json");
        return tags;
    } else if (fixtureType === "series") {
        const series = require("./series.json");
        return series[id];
    }
};

export default accessFixtureData;
