import * as React from "react";
import * as Redux from "redux";
import { connect } from "react-redux";
import { actionCreators } from "../state/actions/actions";
import { selectCurrentTag } from "../state/store/selectors";

import FrontPage from "./pages/FrontPage";
import DetailPage from "./pages/DetailPage";

interface IAppProps {
    currentTag: string | void;
}

// TODO: Would replace this with more robust routing
class AppComponent extends React.Component<IAppProps, {}> {
    public render(): JSX.Element {
        const tag = this.props.currentTag;

        return (
            <div className="page-wrap">
                { tag
                    ? <DetailPage tag={tag} />
                    : <FrontPage />  }
            </div>
            );
    }
}

const mapStateToProps = (state: IState): IAppProps => {
    return {
        currentTag : selectCurrentTag(state),
    };
};

const mapDispatchToProps = (dispatch: Redux.Dispatch<any>) => {
    return {
        actions: Redux.bindActionCreators(actionCreators, dispatch),
    };
};

export const App = connect<IAppProps>(mapStateToProps, mapDispatchToProps)(AppComponent);

export default App;
