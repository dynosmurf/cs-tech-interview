import * as React from "react";
import * as Redux from "redux";
import { connect } from "react-redux";
import { actionCreators } from "../../state/actions/actions";
import { selectXMin, selectXMax, selectActiveTags,
    selectDataPointsInActiveRange, selectCurrentTag,
    selectCurrentChartType } from "../../state/store/selectors";

import { LineChart } from "../series/LineChart";
import { StatusChart } from "../series/StatusChart";
import { XAxis } from "../series/XAxis";
import { YAxis } from "../series/YAxis";
import { ControlPane } from "../series/ControlPane";
import { DataPoint } from "../../state/models/DataSeries";

import TagList from "../tag/TagList";

import * as dfns from "date-fns";

import DateTimePicker from "react-datetime-picker";

import "./DetailPage.less";

interface IDetailPageProps {
    actions?: any;
    tag: any;
    tagId: string;
    xMin: Date;
    xMax: Date;
    yMin: number;
    yMax: number;
    series: DataPoint[];
}

class DetailPageComponent extends React.Component<IDetailPageProps, {}> {
    constructor() {
        super();

        this.state = {
            xMin : null,
        };
    }

    public render(): JSX.Element {
        const {isLoading, chartType, xMax, xMin, yMax, yMin, series } = this.props;
        const controlXMin = this.state.xMin || xMin;
        const controlXMax = this.state.xMax || xMax;

        if (isLoading) {
            return ( <div className="loading"></div>);
        }

        return (
            <div className="detailpage-container">
                <div className="detailpage-sidebar">
                    <a className="back-button"
                        onClick={() => {this.props.actions.clearTagAction(); }}>back</a>
                    <TagList tags={this.props.tags} handleClick={this.handleSetTag}/>
                </div>

                <div className="detailpage-main">
                    <div className="detailpage-controls">
                        <DateTimePicker
                            onChange={this.handleFromDateChange.bind(this)}
                            value={controlXMin}/>

                        <DateTimePicker
                            onChange={this.handleToDateChange.bind(this)}
                            value={controlXMax}/>

                        <span className="submit"
                            onClick={this.handleUpdateRange.bind(this)}>
                            Update
                        </span>
                    </div>

                    <div className="detailpage-header">
                        <h3>{this.props.tag.label}</h3>
                    </div>

                    {this.renderStage()}

                </div>
            </div>
        );
    }

    public renderStage(): JSX.Element {

        const {isLoading, chartType, xMax, xMin, yMax, yMin, series } = this.props;
        const leftMargin = 60;
        const unit = this.props.tag.unit;
        return (
            <div className="detailpage-chartstage">

                { chartType === "line" || chartType === "status" ?
                <LineChart key={this.props.tagId}
                    xMax={xMax} xMin={xMin}
                    yMax={yMax} yMin={yMin}
                    series={series}
                    leftMargin={leftMargin}
                    bottomMargin={40} /> : null }

                { /*
                { chartType === "status" ?
                <StatusChart
                    xMax={xMax} xMin={xMin}
                    yMax={yMax} yMin={yMin}
                    series={series}
                    leftMargin={leftMargin}
                    bottomMargin={40} /> : null }
                   */ }

                <XAxis xMax={xMax} xMin={xMin} leftMargin={leftMargin} bottomMargin={40}/>

                { chartType === "line" ?
                <YAxis yMax={yMax}
                    label={unit}
                    yMin={yMin}
                    leftMargin={leftMargin}
                    bottomMargin={40}/>
                : null }

                <ControlPane
                    onScrollHorizontal={(delta, x, y) => { this.handleScrollHorizontal(delta, x, y); }}
                    onScrollVertical={(delta, x, y) => { this.handleScrollVertical(delta, x, y); }} />
            </div>
        );
    }

    private handleSetTag = (tagId: string) => {
        this.props.actions.setTagAction(tagId);
    }

    private handleFromDateChange(date) {
        this.setState({
            xMin : date,
        });
    }

    private handleToDateChange(date) {
        this.setState({
            xMax: date,
        });
    }

    private handleScrollHorizontal(delta, x, y) {
        let controlXMin = this.state.xMin || this.props.xMin;
        let controlXMax = this.state.xMax || this.props.xMax;
        const diff = dfns.differenceInMilliseconds(new Date(controlXMax), new Date(controlXMin));
        controlXMin = dfns.addMilliseconds(new Date(controlXMin),  delta / 500 * diff);
        controlXMax = dfns.addMilliseconds(new Date(controlXMax),  delta / 500 * diff);

        this.props.actions.updateRangeAction(this.props.tagId, controlXMin, controlXMax);
    }

    private handleScrollVertical(delta, x, y) {
        let controlXMin = this.state.xMin || this.props.xMin;
        let controlXMax = this.state.xMax || this.props.xMax;
        const diff = dfns.differenceInMilliseconds(new Date(controlXMax), new Date(controlXMin));
        controlXMin = dfns.addMilliseconds(new Date(controlXMin),  delta / 500 * diff);
        controlXMax = dfns.addMilliseconds(new Date(controlXMax),  -1 * delta / 500 * diff);

        this.props.actions.updateRangeAction(this.props.tagId, controlXMin, controlXMax);
    }

    private handleUpdateRange() {
        const controlXMin = this.state.xMin || this.props.xMin;
        const controlXMax = this.state.xMax || this.props.xMax;
        this.props.actions.updateRangeAction(this.props.tagId, controlXMin, controlXMax);
        this.setState({
            xMin: null,
            xMax: null,
        });
    }
}

const mapStateToProps = (state: IState): IDetailPageProps => {
    const series = selectDataPointsInActiveRange(state);
    const yMax = series.reduce((acc, p) => {
        return acc = acc < p.value ? p.value : acc;
    }, 0);

    const yMin = series.reduce((acc, p) => {
        return acc = acc > p.value ? p.value : acc;
    }, Infinity);

    const currentTag = selectCurrentTag(state);

    return {
        isLoading: state.controls.loading,
        tags: selectActiveTags(state),
        tagId: selectCurrentTag(state),
        tag: state.tags[currentTag],
        xMin: selectXMin(state),
        xMax: selectXMax(state),
        yMin,
        yMax,
        series,
        chartType: selectCurrentChartType(state),
    };
};

const mapDispatchToProps = (dispatch: Redux.Dispatch<any>) => {
    return {
        actions: Redux.bindActionCreators(actionCreators, dispatch),
    };
};

export const DetailPage = connect<IDetailPageProps>(mapStateToProps, mapDispatchToProps)(DetailPageComponent);

export default DetailPage;
