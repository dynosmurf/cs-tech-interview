import * as React from "react";
import * as Redux from "redux";
import { connect } from "react-redux";
import { actionCreators } from "../../state/actions/actions";
import { TagItem } from "../../state/models/TagItem";
import { selectActiveTags, selectFeatures, selectActiveFeatures } from "../../state/store/selectors";

import TagList from "../tag/TagList";
import TagFeatureFilter from "../tag/TagFeatureFilter";

import "./FrontPage.less";

interface IFrontPageProps {
    actions?: any;
    features: string[];
    activeFeatures: string[];
    tags: TagItem[];
}

class FrontPageComponent extends React.Component<IFrontPageProps, {}> {

    constructor(props: IFrontPageProps) {
        super(props);
        this.handleToggleFilter = this.handleToggleFilter.bind(this);
    }

    public render(): JSX.Element {

        if (this.props.isLoading) {
            return ( <div className="loading"></div> );
        }

        return (
            <div className="frontpage-container">
                <div className="frontpage-main">
                    <h2 className="frontpage-header">Tags</h2>
                    <TagFeatureFilter
                        handleFilter={this.handleToggleFilter.bind(this)}
                        activeFeatures={this.props.activeFeatures}
                        features={this.props.features} />

                    <TagList handleClick={this.handleSetTag.bind(this)}
                        tags={this.props.tags} />
                </div>
            </div>
        );
    }

    private handleToggleFilter = (features: string[]) => {
        this.props.actions.toggleFeatureFilterAction(features);
    }

    private handleSetTag = (tagId: string) => {
        this.props.actions.setTagAction(tagId);
    }
}

// TODO : consider implementing tag selector
const mapStateToProps = (state: IState): IFrontPageProps => {
    return {
        tags: selectActiveTags(state),
        features: selectFeatures(state),
        activeFeatures: selectActiveFeatures(state),
        isLoading: state.controls.loading,
    };
};

// TODO : make this more selective don't need all actions
const mapDispatchToProps = (dispatch: Redux.Dispatch<any>) => {
    return {
        actions: Redux.bindActionCreators(actionCreators, dispatch),
    };
};

export const FrontPage = connect<IFrontPageProps>(mapStateToProps, mapDispatchToProps)(FrontPageComponent);

export default FrontPage;
