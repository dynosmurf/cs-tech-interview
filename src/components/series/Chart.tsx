import * as React from "react";
import ReactResizeDetector from "react-resize-detector";
import * as d3 from "d3";

import { DataPoint } from "../../state/models/DataSeries";

export interface ChartProps {
    yMax: number;
    yMin: number;
    xMax: number;
    xMin: number;
    series: DataPoint[];
}

export class Chart extends React.Component<ChartProps, {}> {
    constructor() {
        super();
        this.state = {};
        this.svgRef = React.createRef();
    }

    public render() {
        const { width, height } = this.props;
        return (
            <div className="linechart-cont" ref={this.sizeRef}>
                <svg ref={this.svgRef} width={this.state.width} height={this.state.height} />
                <ReactResizeDetector
                    handleWidth
                    handleHeight
                    onResize={this.onResize.bind(this)} />
            </div>
        );
    }

    public componentDidUpdate() {
        const { xMin, yMin, xMax, yMax } = this.props;
        const height = this.svgRef.current.clientHeight;
        const width = this.svgRef.current.clientWidth;
        this.update(xMin, yMin, xMax, yMax, width, height);
    }

    public componentDidMount() {
        const { xMin, yMin, xMax, yMax } = this.props;
        /* resize will not have been called and so we need to get the width
         * and height maually */
        const height = this.svgRef.current.clientHeight;
        const width = this.svgRef.current.clientWidth;
        this.draw(xMin, yMin, xMax, yMax, width, height);
    }

    private onResize(width, height) {
        const { xMin, yMin, xMax, yMax } = this.props;
        this.update(xMin, yMin, xMax, yMax, width, height);
        this.setState({
            width, height,
        });
    }

    private setScales(xMin, yMin, xMax, yMax, width, height) {
        this.xScale = d3.scaleTime()
            .domain([xMin, xMax])
            .range([this.props.leftMargin, width]);

        // set up the y scale
        this.yScale = d3.scaleLinear()
            .domain([yMin, yMax])
            .range([height - this.props.bottomMargin, 0]);

        let xOffset = height * ( yMax / (yMax - yMin) );
        xOffset = xOffset > height ? height - 20 : xOffset;
        xOffset = xOffset < 0 ? 20 : xOffset;
        this.xOffset = xOffset;

        let yOffset = width - width * ( xMax / (xMax - xMin) );
        yOffset = yOffset > width ? width - 20 : yOffset;
        yOffset = yOffset < 0 ? 20 : yOffset;
        this.yOffset = yOffset;
    }
}
