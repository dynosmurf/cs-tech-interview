import * as React from "react";
import ReactResizeDetector from "react-resize-detector";
import * as d3 from "d3";

import { DataPoint } from "../../state/models/DataSeries";

import { Chart } from "./Chart";

import "./LineChart.less";

export interface LineChartProps {
    yMax: number;
    yMin: number;
    xMax: number;
    xMin: number;
    series: DataPoint[];
}

export class LineChart extends Chart<LineChartProps, {}> {
    constructor() {
        super();
    }

    private draw(xMin, yMin, xMax, yMax, width, height) {
        const svgEl = this.svgRef.current;

        const svg = d3.select(svgEl)
            .append("g");

        this.setScales(xMin, yMin, xMax, yMax, width, height);

        const lineBuilder = d3.line()
            .x((d) => this.xScale(new Date(d.ts)))
            .y((d) => this.yScale(d.value));

        const lineGroup = svg.append("path")
            .attr("class", "linechart-line")
            .attr("d", lineBuilder(this.props.series));
    }

    private update(xMin, yMin, xMax, yMax, width, height) {
        const svgEl = this.svgRef.current;

        this.setScales(xMin, yMin, xMax, yMax, width, height);

        const lineBuilder = d3.line()
            .x((d) => this.xScale(new Date(d.ts)))
            .y((d) => {
                return this.yScale(d.value);
            });

        const path = d3.select(svgEl)
            .selectAll("path")
            .attr("d", lineBuilder(this.props.series));
    }
}
