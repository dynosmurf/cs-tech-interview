import * as React from "react";
import ReactResizeDetector from "react-resize-detector";
import * as d3 from "d3";

import "./XAxis.less";

export interface XAxisProps {
    xMax: number;
    xMin: number;
    height: number;
}

export class XAxis extends React.Component<XAxisProps, {}> {
    constructor() {
        super();
        this.state = {};
        this.sizeRef = React.createRef();
        this.svgRef = React.createRef();
    }

    public render() {
        const { width } = this.props;
        return (
            <div className="xaxis-cont" ref={this.sizeRef}>
                <svg ref={this.svgRef} width={this.state.width} height={this.props.bottomMargin} />
                <ReactResizeDetector
                    handleWidth
                    onResize={this.onResize.bind(this)} />
            </div>
        );
    }

    public componentDidUpdate() {
        const { xMin, xMax } = this.props;
        const { width } = this.state;
        this.update(xMin, xMax, width);
    }

    public componentDidMount() {
        const { xMin, xMax } = this.props;
        /* resize will not have been called and so we need to get the width
         * and height maually */
        const width = this.sizeRef.current.clientWidth;
        this.draw(xMin, xMax, width);
    }

    private setScales(xMin, xMax, width) {
        this.xScale = d3.scaleTime()
            .domain([xMin, xMax])
            .range([this.props.leftMargin, width]);

        this.xAxis = d3.axisBottom()
            .scale(this.xScale);

        this.xOffset = 0;
    }

    private draw(xMin, xMax, width) {
        const svgEl = this.svgRef.current;

        const svg = d3.select(svgEl)
            .append("g");

        const xAxisEl = svg.append("g")
            .attr("class", "axes-xAxis");

        this.setScales(xMin, xMax, width);
        this.updateX(xAxisEl);
    }

    private updateX(xAxisGroupEl) {
        xAxisGroupEl
            .attr("transform", "translate(0, " + this.xOffset + ")")
            .call(this.xAxis);
    }

    private update(xMin, xMax, width) {
        const svgEl = this.svgRef.current;
        const xAxisEl = d3.select(svgEl).select("g.axes-xAxis");

        this.setScales(xMin, xMax, width);
        this.updateX(xAxisEl);
    }

    private onResize(width, height) {
        const { xMin, xMax } = this.props;
        this.update(xMin, xMax, width);
        this.setState({
            width,
        });
    }
}
