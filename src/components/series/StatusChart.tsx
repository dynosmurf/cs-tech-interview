import * as React from "react";
import ReactResizeDetector from "react-resize-detector";
import * as d3 from "d3";

import { DataPoint } from "../../state/models/DataSeries";

import { Chart } from "./Chart";

import "./StatusChart.less";

export interface StatusChartProps {
    yMax: number;
    yMin: number;
    xMax: number;
    xMin: number;
    series: DataPoint[];
}

export class StatusChart extends Chart<StatusChartProps, {}> {
    constructor() {
        super();
    }

    private draw(xMin, yMin, xMax, yMax, width, height) {
        const svgEl = this.svgRef.current;

        this.setScales(xMin, yMin, xMax, yMax, width, height);

        const data = this.processData(this.props.series);

        const svg = d3.select(svgEl)
            .append("g")
            .data(data)
            .selectAll("rect")
            .enter().append("rect")
            .attr("x", (d) => this.xScale(d.startX))
            .attr("width", (d) => (this.xScale(d.endX) - this.xScale(d.startX)))
            .attr("height", "50%");

    }

    private update(xMin, yMin, xMax, yMax, width, height) {
        const svgEl = this.svgRef.current;

        this.setScales(xMin, yMin, xMax, yMax, width, height);

        const data = this.processData(this.props.series);

        const rects = d3.select(svgEl).select("g")
            .selectAll("rect")
            .data(data)
            .attr("x", (d) => this.xScale(new Date(d.startX)))
            .attr("width", (d) => {
                return Math.abs(this.xScale(new Date(d.endX)) - this.xScale(new Date(d.startX)));
            });

        rects.enter().append("rect")
            .attr("x", (d) => this.xScale(new Date(d.startX)))
            .attr("width", (d) => {
                return Math.abs(this.xScale(new Date(d.endX)) - this.xScale(new Date(d.startX)));
            })
            .attr("class", (d) => this.classGen(d.value))
            .attr("height", "50%");

        rects.exit().remove();
    }

    private processData() {
        const comp = (a, b) => {
            if (a.date < b.date) {
                return 1;
            }
            if (a.date > b.date) {
                return -1;
            }
            return 0;
        };

        const working = [...this.props.series];
        working.sort(comp);
        const out = [];

        let lastV = null;
        const getAcc = (p) => ({ value: p.value, startX : p.ts });
        let acc = null;

        for (const p of working) {
            const nextV = p.value;
            if ( nextV !== lastV ) {
                if (acc) {
                    acc.endX = p.ts;
                    out.push(acc);
                }
                acc = getAcc(p);
            }
            lastV = nextV;
        }
        if (acc) {
            out.push(acc);
        }

        return out;
    }

    private classGen(value) {
        if (value === false) {
            return "status-off";
        }
        if (value === true) {
            return "status-on";
        }
        // TODO : would want to sanatize this better
        return `status-${value.toLowerCase()}`;
    }
}
