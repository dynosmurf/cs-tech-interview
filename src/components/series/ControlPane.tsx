import * as React from "react";
import ReactResizeDetector from "react-resize-detector";
import * as d3 from "d3";

import "./ControlPane.less";

export interface ControlPaneProps {
    onScrollVertical?(this: ControlPane, delta);
    onScrollHorizontal?(this: ControlPane, delta);
    onDragStart?(this: ControlPane, dragX: number, dragY: number);
    onDragEnd?(this: ControlPane, dragX: number, dragY: number);
    onDrag?(this: ControlPane, dragX: number, dragY: number);
    onHover?(this: ControlPane, hoverX: number, hoverY: number);
}

export class ControlPane extends React.Component<ControlPaneProps, {}> {
    constructor() {
        super();
        this.state = {
            dragging : false,
        };
        this.boundHandleGlobalMouseUp = this.handleGlobalMouseUp.bind(this);
    }

    public render() {
        return (
            <div className="controlpane"
                onMouseDown={this.handleOnDragStart.bind(this)}
                onMouseUp={this.handleOnDragEnd.bind(this)}
                onMouseMove={this.handleOnDrag.bind(this)}
                onWheel={this.handleOnScroll.bind(this)}>
            </div>
        );
    }

    private handleOnScroll(e) {
        if (this.props.onScrollHorizontal && e.deltaX !== 0) {
            this.props.onScrollHorizontal.call(this, e.deltaX, e.clientX, e.clientY);
        }
        if (this.props.onScrollVertical && e.deltaY !== 0) {
            this.props.onScrollVertical.call(this, e.deltaY, e.clientX, e.clientY);
        }
    }

    private handleGlobalMouseUp(e) {
        document.removeEventListener("mouseup", this.boundHandleGlobalMouseUp);
        this.handleOnDragEnd(e);
    }

    private handleOnDragStart(e) {
        document.addEventListener("mouseup", this.boundHandleGlobalMouseUp);

        const x = e.clientX;
        const y = e.clientY;

        this.setState({
            dragging : true,
        });

        if (this.props.onDragStart) {
            this.props.onDragStart.call(this, x, y);
        }
    }

    private handleOnDragEnd(e) {
        const x = e.clientX;
        const y = e.clientY;

        this.setState({
            dragging: false,
        });

        if (this.props.onDragEnd) {
            this.props.onDragEnd.call(this, x, y);
        }
    }

    private handleOnDrag(e) {
        if (this.props.onHover) {
            this.handleOnHover(e);
        }

        if (this.props.onDrag && this.state.dragging ) {
            const x = e.clientX;
            const y = e.clientY;
            this.props.onDrag.call(this, x, y);
        }
    }

    private handleOnHover(e) {
        const x = e.clientX;
        const y = e.clientY;
        this.props.onHover.call(this, x, y);
    }
}
