import * as React from "react";
import ReactResizeDetector from "react-resize-detector";
import * as d3 from "d3";

import "./YAxis.less";

export interface YAxisProps {
    yMax: number;
    yMin: number;
    height: number;
}

export class YAxis extends React.Component<YAxisProps, {}> {
    constructor() {
        super();
        this.state = {};
        this.sizeRef = React.createRef();
        this.svgRef = React.createRef();
    }

    public render() {
        const { height, label } = this.props;
        return (
            <div className="yaxis-cont" ref={this.sizeRef}>
                <div className="yaxis-label">
                    {label}
                </div>
                <svg ref={this.svgRef} height={this.state.height} width={this.props.width} />
                <ReactResizeDetector
                    handleHeight
                    onResize={this.onResize.bind(this)} />
            </div>
        );
    }

    public componentDidUpdate() {
        const { yMin, yMax } = this.props;
        const { height } = this.state;
        this.update(yMin, yMax, height);
    }

    public componentDidMount() {
        const { yMin, yMax } = this.props;
        /* resize will not have been called and so we need to get the height
         * and height maually */
        const height = this.sizeRef.current.clientHeight;
        this.draw(yMin, yMax, height);
    }

    private setScales(yMin, yMax, height) {
        this.yScale = d3.scaleLinear()
            .domain([yMin, yMax])
            .range([height - this.props.bottomMargin, 0]);

        this.yAxis = d3.axisLeft()
            .scale(this.yScale);

        this.yOffset = this.props.leftMargin;
    }

    private draw(yMin, yMax, height) {
        const svgEl = this.svgRef.current;

        const svg = d3.select(svgEl)
            .append("g");

        const yAxisEl = svg.append("g")
            .attr("class", "axes-yAxis");

        this.setScales(yMin, yMax, height);
        this.updateY(yAxisEl);
    }

    private updateY(yAxisGroupEl) {
        yAxisGroupEl.attr("transform", "translate(" + this.yOffset + ", 0)")
            .call(this.yAxis);
    }

    private update(yMin, yMax, height) {
        const svgEl = this.svgRef.current;
        const yAxisEl = d3.select(svgEl).select("g.axes-yAxis");

        this.setScales(yMin, yMax, height);
        this.updateY(yAxisEl);
    }

    private onResize(width, height) {
        const { yMin, yMax } = this.props;
        this.update(yMin, yMax, height);
        this.setState({
            height,
        });
    }
}
