import * as React from "react";

import "./TagFeatureFilter.less";

export interface ITagFeatureFilterProps {
    features: string[];
    activeFeatures: string[];
    handleFilter(feature: string);
}

class TagFeatureFilter extends React.Component<ITagFeatureFilterProps, {}> {
    public render(): JSX.Element {
        return (
            <div className="tagfeaturefilter-container">
                <h4>Filter by Feature</h4>
                <div className="filters">
                    <div onClick={() => {this.props.handleFilter(this.props.features); }}
                        className="feature-filter">all</div>
                    { this.props.features.map( (f) => {
                        return (
                            <div key={f}
                                className={"feature-filter " + (this.isActive(f) ? "active" : "inactive")}
                                onClick={() => {this.props.handleFilter([f]); }}>
                                {f}
                            </div>
                            );
                    })}
            </div>
        </div>
        );
    }

    private isActive(feature) {
        return this.props.activeFeatures.includes(feature);
    }
}

export default TagFeatureFilter;
