import * as React from "react";
import { Link } from "react-router-dom";
import { TagItem } from "../state/models/TagItem.tsx";

// import { TagListItem } from "./TagListItem.tsx";
import "./TagList.less";

interface ITagListProps {
    tags: TagItem[];
}

class TagList extends React.Component<ITagListProps, {}> {
    public render(): JSX.Element {
        return (
            <div className="taglist-container">
                 <div className="taglist">
                     { this.props.tags.map( (tag) => {
                         return (
                             <div className="taglist-item"
                                 onClick={() => { this.props.handleClick(tag.tagId); }}
                                 key={tag.tagId}>
                                 <h3>{tag.label}</h3>
                                 <div className="tag-features">
                                     {tag.features.map( (f) => {
                                         return (
                                             <span className="tag-feature"
                                                 key={`${tag.tagId}-${f}`}>
                                                 {f}
                                             </span>
                                             );
                                     })}
                                 </div>
                             </div>
                         );
                     })}
                </div>
            </div>
        );
    }
}

export default TagList;
